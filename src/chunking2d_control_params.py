from models.chunking2d import *
from util import get_durations, permute
import numpy as np
import math
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.gridspec as gridspec
from icecream import ic
plt.subplots_adjust(left=0, bottom=0, right=0.97, top=0.93)

M = 3
N_k = 3
dt = 0.1
ITER_PER_RUN = 10000
NUM_TESTS = 3000 # ^ 3
TRIM_CYCLES = 0

beta = 0.01
sigma = np.random.rand(3,3)*5 + 5

#rs = np.random.rand(NUM_TESTS, 3)

# Independent test of r2 and r3
rs = np.ones((NUM_TESTS, 3)) * 0.5
half_tests = int(NUM_TESTS/2)
#r2s = np.vstack((np.random.rand(half_tests, 3), np.ones((half_tests, 3))*0.5))
#r3s = np.vstack((np.ones((half_tests, 3))*1.4, np.random.rand(half_tests, 3) + 1))
r2s = np.vstack((np.ones((half_tests, 3))*0.5, np.random.rand(half_tests, 3)*3-1.5))
r3s = np.vstack((np.random.rand(half_tests, 3)*2 + 1, np.ones((half_tests, 3))*1.4))
ic(r2s)
ic(r3s)

R2_MAX = 1
R2_MIN = 0
R3_MAX = 2
R3_MIN = 1

"""
r2s = permute(R2_MIN, R2_MAX, (half_tests, 3))
r2s = np.vstack((r2s, np.ones((NUM_TESTS-r2s.shape[0], 3))*0.5))
r3s = permute(R3_MIN, R3_MAX, (half_tests, 3))
r3s = np.vstack((np.ones((NUM_TESTS-r3s.shape[0], 3))*1.4, r3s))
"""

avg_peaks = []
d_peaks = []
for n in range(NUM_TESTS):
    ic(r2s[n], r3s[n])
    # Fill roe
    l_roe = np.repeat(np.zeros((3,3))[np.newaxis,:,:], M, axis=0)
    for k in range(M):
        for i in range(N_k):
            l_roe[k,i,i] = 1
            l_roe[k,(i-1)%M,i] = sigma[(i-1)%M,k]/sigma[i,k] + 0.51#rs[n, k]
            l_roe[k,(i+1)%M,i] = sigma[(i+1)%M,k]/sigma[i,k] - 0.5#rs[n, k]

    # Fill epsilon
    epsilon = np.zeros((3,3))
    ic(r3s[n])
    for k in range(M):
        epsilon[k, k] = 1
        epsilon[k, (k+1)%M] = r3s[n, k]
        epsilon[k, (k-1)%M] = r2s[n, k]

    model = Model(sigma, l_roe, epsilon, beta, 0.7, 2.0, M, N_k)

    # Log
    js = np.zeros((3))
    xs = [np.zeros((3))] * 3
    ys = np.zeros((3))

    # Collect
    space = range(ITER_PER_RUN)
    for i in space:
        model.update(dt)
        # log
        js = np.vstack((js, model.js()))
        for i in range(3):
            xs[i] = np.vstack((xs[i], model.x[:,i]))
        ys = np.vstack((ys, model.y))


    # Durations
    peaks = []
    for i in range(ys.shape[1]):
        p, v = get_durations(ys[20:,i], lambda x: x > 0.5)
        if p.shape[0] == 0:
            continue
        peaks.append(p)

    if len(peaks) != 3 or len(peaks[0]) < TRIM_CYCLES or len(peaks[2]) < TRIM_CYCLES or len(peaks[2]) < TRIM_CYCLES:
        avg_peaks.append([0,0,0])
    else:
        avg_peaks.append([
            dt*np.median(peaks[0][TRIM_CYCLES:,0]),
            dt*np.median(peaks[1][TRIM_CYCLES:,0]),
            dt*np.median(peaks[2][TRIM_CYCLES:,0]) ])
    # Changes in duration
    if len(peaks) != 3 or \
       len(peaks[0]) < TRIM_CYCLES+1 or \
       len(peaks[2]) < TRIM_CYCLES+1 or \
       len(peaks[2]) < TRIM_CYCLES+1:
        d_peaks.append([0,0,0])
    else:
        d_peaks.append([
            dt*np.median(peaks[0][TRIM_CYCLES+1:,0] - peaks[0][TRIM_CYCLES:-1,0]),
            dt*np.median(peaks[1][TRIM_CYCLES+1:,0] - peaks[1][TRIM_CYCLES:-1,0]),
            dt*np.median(peaks[2][TRIM_CYCLES+1:,0] - peaks[2][TRIM_CYCLES:-1,0])])
    """
    # Plot all rows
    titles = ["Y"] + ["X%i" % (i+1) for i in range(N_k)]
    #titles = ["Y"] + ["X with r: %f" % r[i] for i in range(N_k)]
    xlabels = ["time"] * 4
    ylabels = ["activation"] * 4
    datas = [ys] + xs

    fig = plt.figure(1)
    ic(
        (peaks[0][TRIM_CYCLES:,0]),
        (peaks[1][TRIM_CYCLES:,0]),
        (peaks[2][TRIM_CYCLES:,0]))
    ic(
        (peaks[0][TRIM_CYCLES+1:,0] - peaks[0][TRIM_CYCLES:-1,0]),
        (peaks[1][TRIM_CYCLES+1:,0] - peaks[1][TRIM_CYCLES:-1,0]),
        (peaks[2][TRIM_CYCLES+1:,0] - peaks[2][TRIM_CYCLES:-1,0]))

    for i in range(len(titles)):
        plt.subplot2grid((4,6), (i,2), colspan=4, rowspan=1)
        fig.get_axes()[i].set_title(titles[i])
        fig.get_axes()[i].set_xlabel(xlabels[i])
        fig.get_axes()[i].set_ylabel(ylabels[i])
        plt.plot(space, datas[i][1:,0], c='r')
        plt.plot(space, datas[i][1:,1], c='g')
        plt.plot(space, datas[i][1:,2], c='b')
    plt.show()
    """

avg_peaks = np.array(avg_peaks)
d_peaks = np.array(d_peaks)

# Plot 3d projection
#plt.subplot2grid((4,6), (0,0), colspan=2, rowspan=2, projection='3d')
#ax3 = plt.get_axes()[0]
ax1 = plt.subplot(221, projection='3d')
ax1.set_title("Change in peak size wrt r2, ps=peak size")
ax1.set_xlabel("ps Y mode 1")
ax1.set_ylabel("ps Y mode 2")
ax1.set_zlabel("ps Y mode 3")
ax1.quiver(
        r2s[:,0], 
        r2s[:,1], 
        r2s[:,2],
        avg_peaks[:,0], 
        avg_peaks[:,1], 
        avg_peaks[:,2], 
        length=5)

ax2 = plt.subplot(222, projection='3d')
ax2.set_title("Change in peak size/dt wrt r2")
ax2.set_xlabel("ps/dt Y mode 1")
ax2.set_ylabel("ps/dt Y mode 2")
ax2.set_zlabel("ps/dt Y mode 3")
ax2.quiver(
        r2s[:,0], 
        r2s[:,1], 
        r2s[:,2], 
        d_peaks[:,0], 
        d_peaks[:,1], 
        d_peaks[:,2], 
        length=.08)

ax3 = plt.subplot(223, projection='3d')
ax3.set_title("Change in peak size wrt r3")
ax3.set_xlabel("ps Y mode 1")
ax3.set_ylabel("ps Y mode 2")
ax3.set_zlabel("ps Y mode 3")
ax3.quiver(
        r3s[:,0], 
        r3s[:,1], 
        r3s[:,2], 
        avg_peaks[:,0], 
        avg_peaks[:,1], 
        avg_peaks[:,2], 
        length=3)

ax4 = plt.subplot(224, projection='3d')
ax4.set_title("Change in peak size/dt wrt r3")
ax4.set_xlabel("ps/dt Y mode 1")
ax4.set_ylabel("ps/dt Y mode 2")
ax4.set_zlabel("ps/dt Y mode 3")
ax4.quiver(
        r3s[:,0], 
        r3s[:,1], 
        r3s[:,2], 
        d_peaks[:,0], 
        d_peaks[:,1], 
        d_peaks[:,2], 
        length=.05)

plt.show()

suffix = "_controlled_boundary"

with open("../data/avg_peaks%s.npy"%suffix, "wb+") as f:
    np.save(f, avg_peaks)
with open("../data/d_peaks%s.npy"%suffix, "wb+") as f:
    np.save(f, d_peaks)
with open("../data/rs%s.npy"%suffix, "wb+") as f:
    np.save(f, rs)
with open("../data/r2s%s.npy"%suffix, "wb+") as f:
    np.save(f, r2s)
with open("../data/r3s%s.npy"%suffix, "wb+") as f:
    np.save(f, r3s)
