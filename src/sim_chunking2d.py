from models.chunking2d import *
import numpy as np
import math, util
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.gridspec as gridspec
from icecream import ic
import params, util

M = 6
N_k = 6
SECONDS = 200
dt = 0.010
iters = int(SECONDS/dt)

sigma = np.random.rand(N_k, M)*5 + 5

beta = 0.01

controls = (0.5, 0.5, 0.5, 0.5, 1.5, 0.1, 1)

roe, epsilon, cs = params.gen_params((N_k, M), sigma, beta=beta, controls=controls)

tao = np.random.rand(M)*2
#beta = 0.01
model = Model(sigma, roe, epsilon, beta, 0.7, tao, M, N_k)
print(str(model))

# Log
js = []
#xs = np.zeros((N_k,M))
xs = [[] for i in range(N_k)]
ys = []
zs = []

xs_dt = []
ys_dt = []
zs_dt = []

# Run
space = np.linspace(0, SECONDS, iters)
for i in space:
    model.update(dt)
    # log
    js.append(np.copy(model.js()))
    #xs = np.vstack((xs, model.x))
    for j in range(N_k):
        #ic(len(xs[j]), j, i)
        xs[j].append(np.copy(model.x[:,j]))
    ys.append(np.copy(model.y))
    zs.append(np.copy(model.z))

    xs_dt.append(model.x_prime(model.x))
    ys_dt.append(model.y_prime(model.y))
    zs_dt.append(model.z_prime(model.z))

js = np.array(js)
ys = np.array(ys)
zs = np.array(zs)
xs_dt = np.array(xs_dt)
ys_dt = np.array(ys_dt)
zs_dt = np.array(zs_dt)

# Integrate zs - 1
# upper triangle * zs
#integrator = np.tril(np.ones((iters, iters)))
#zs_int = integrator.dot(1-zs)

# derive ys_dt
ys_ddt = np.vstack((np.zeros_like(ys_dt[1]), ys_dt[1:]-ys_dt[:-1]))

# Plot
fig = plt.figure(1)
rowsize = (1, 4)

# Plot all rows
#titles = ["Z deriv"] + ["Z"] + ["Z integral"] + ["Y deriv"] + ["Y"]# + ["X%i" % (i+1) for i in range(N_k)]
#titles = ["Z deriv"] + ["Z"] + ["Y 2deriv"] + ["Y deriv"] + ["Y"]# + ["X%i" % (i+1) for i in range(N_k)]
titles = ["Z"] + ["Y"] + ["X%i" % (i+1) for i in range(M)]
xlabels = ["time"] * 20
ylabels = ["activation"] * 20
#datas = [zs_dt, zs, zs_int, ys_dt, ys, xs1, xs2, xs3]
#datas = [zs_dt, zs, ys_ddt, ys_dt, ys, xs1, xs2, xs3]
ic(len(xs), len(titles))

datas = [zs, ys, xs[0], xs[1], xs[2], xs[3], xs[4], xs[5]]
for i in range(len(datas)):
    datas[i] = np.array(datas[i])

# Plot 3d projection
size = util.get_size((0, 2), rowsize, len(titles))
plt.subplot2grid(size, (0,0), colspan=2, rowspan=2, projection='3d')
fig.get_axes()[0].set_title("Projection")
fig.get_axes()[0].set_xlabel("J1")
fig.get_axes()[0].set_ylabel("J2")
fig.get_axes()[0].set_zlabel("J3")
plt.plot(js[1:,0], js[1:,1], js[1:,2])

for i in range(len(titles)):
    ax = util.subplot2grid(plt, (0, 2), rowsize, len(titles), i)
    ax.set_title(titles[i])
    ax.set_xlabel(xlabels[i])
    ax.set_ylabel(ylabels[i])
    palette = util.create_color_palette(datas[i].shape[1])
    for j in range(datas[i].shape[1]):
        ax.plot(space, datas[i][:,j], c=palette[j])
plt.show()
