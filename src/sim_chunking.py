from models.hack_chunking import *
import numpy as np
import math
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.gridspec as gridspec
from icecream import ic

M = 3
N_k = 4

sigma = np.random.rand(N_k)*5 + 5

l_roe = np.zeros((N_k,N_k))

# Fill roe
for i in range(N_k):
    l_roe[i,i] = 1
    min_roe = (sigma[(i-1)%N_k])/sigma[i] 
    max_roe = (sigma[(i-1)%N_k]+sigma[i])/sigma[i] 
    l_roe[(i-1)%N_k,i] = np.random.rand(1)*(max_roe-min_roe)+min_roe
    l_roe[(i-1)%N_k,i] = max_roe+np.random.rand(1)*4
    #l_roe[(i-1)%N_k,i] = min_roe+1.4

for i in range(N_k):
    min_roe = (sigma[(i+1)%N_k]+sigma[(i-1)%N_k])/sigma[i] - l_roe[(i-1)%N_k, i]
    max_roe = sigma[(i+1)%N_k]/sigma[i]
    #l_roe[(i+1)%N_k,i] = sigma[(i+1)%N_k]/sigma[i] - 1.0
    l_roe[(i+1)%N_k,i] = np.random.rand(1)*(max_roe-min_roe)+min_roe

model = Model(sigma, l_roe)
print(str(model))

# Log
#js = np.zeros((3))
xs1 = np.zeros((3))
xs2 = np.zeros((3))
xs3 = np.zeros((3))

# Run
space = range(10000)
for i in space:
    model.update(0.01)
    # log
    #js = np.vstack((js, model.js()))
    xs1 = np.vstack((xs1, model.x))

# Plot
fig = plt.figure(1)
size = (1,1)
ic(xs1.shape)

# Plot all rows
titles = ["X%i" % (i+1) for i in range(1)]
ic(len(titles))
xlabels = ["time"] * 4
ylabels = ["activation"] * 4
datas = [xs1]
dim = (size[0],size[1]*len(titles))
#gridspec.GridSpec(*dim)
ic(size)
ic(dim)

for i in range(len(titles)):
    plt.subplot2grid(size, (i,0), colspan=size[0], rowspan=size[1])
    fig.get_axes()[i].set_title(titles[i])
    fig.get_axes()[i].set_xlabel(xlabels[i])
    fig.get_axes()[i].set_ylabel(ylabels[i])
    plt.plot(space, datas[i][1:,0], c='r')
    plt.plot(space, datas[i][1:,1], c='g')
    plt.plot(space, datas[i][1:,2], c='b')
plt.show()
