from models.chunking2d import *
from util import get_durations
import numpy as np
import math
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.gridspec as gridspec
from icecream import ic
import matplotlib as mpl

M = 3
N_k = 3
dt = 0.1
ITERATIONS = 5000
MAX_BETA = 0.05
NUM_TESTS = 100

sigma = np.array( [[7.24, 5.85, 8.30],
                  [9.93, 6.00, 5.18],
                  [8.29, 7.86, 9.16]] )
sigma = np.random.rand(3,3)*5 + 5

l_roe = np.repeat(np.zeros((3,3))[np.newaxis,:,:], M, axis=0)
epsilon = np.zeros((3,3))

# Fill roe
for k in range(M):
    for i in range(N_k):
        l_roe[k,i,i] = 1
        l_roe[k,(i-1)%M,i] = sigma[(i-1)%M,k]/sigma[i,k] + 0.51
        l_roe[k,(i+1)%M,i] = sigma[(i+1)%M,k]/sigma[i,k] - 0.5

# Fill epsilon
for k in range(M):
    epsilon[k, k] = 1
    epsilon[k, (k+1)%M] = 1.4
    epsilon[k, (k-1)%M] = 0.5


#betas = np.linspace(0, 0.01, 500)
betas = np.random.rand(NUM_TESTS, 3) * MAX_BETA
avg_peaks = []
avg_valleys = []

plt.subplot2grid((4,6), (0,0), colspan=2, rowspan=2, projection='3d')
fig = plt.figure(1)
fig.get_axes()[0].set_title("Time space")
fig.get_axes()[0].set_xlabel("ΔT Y1")
fig.get_axes()[0].set_ylabel("ΔT Y2")
fig.get_axes()[0].set_zlabel("ΔT Y3")
num_plots = 3
color = plt.cm.viridis(np.linspace(0, 1, ITERATIONS))
beta_colors = plt.cm.viridis(np.linspace(0, 1, 1000))

for i in range(num_plots):
    plt.subplot2grid((4,6), (i,2), colspan=4, rowspan=1)
    ax = fig.get_axes()[i+1]
    ax.set_title("Y %i" % i)
    ax.set_xlabel("β")
    ax.set_ylabel("ΔT")

for beta in betas:
    model = Model(sigma, l_roe, epsilon, beta, 0.7, 2.0,  M, N_k)
    print(str(model))

    # Log
    js = np.zeros((3))
    xs = [np.zeros((3))] * 3
    ys = np.zeros((3))

    # Collect
    space = range(ITERATIONS)
    for i in space:
        model.update(dt)
        # log
        js = np.vstack((js, model.js()))
        for i in range(3):
            xs[i] = np.vstack((xs[i], model.x[:,0]))
        ys = np.vstack((ys, model.y))

    """
    # Plot all rows
    titles = ["Y"] + ["X%i" % (i+1) for i in range(N_k)]
    xlabels = ["time"] * 4
    ylabels = ["activation"] * 4
    datas = [ys] + xs

    for i in range(len(titles)):
        plt.subplot2grid((4,6), (i,2), colspan=4, rowspan=1)
        fig.get_axes()[i].set_title(titles[i])
        fig.get_axes()[i].set_xlabel(xlabels[i])
        fig.get_axes()[i].set_ylabel(ylabels[i])
        plt.plot(space, datas[i][1:,0], c='r')
        plt.plot(space, datas[i][1:,1], c='g')
        plt.plot(space, datas[i][1:,2], c='b')
    plt.show()
    """

    # Durations
    peaks = []
    for i in range(ys.shape[1]):
        p, v = get_durations(ys[20:,i], lambda x: x > 0.5)
        peaks.append(p)
    avg_peaks.append([
        dt*np.average(peaks[0][:,0]),
        dt*np.average(peaks[1][:,0]),
        dt*np.average(peaks[2][:,0]) ])
    """
    avg_valleys.append([
        dt*np.average(valleys_0[:,0]),
        dt*np.average(valleys_1[:,0]),
        dt*np.average(valleys_2[:,0]) ])
    """
    length = min(peaks[0].shape[0], peaks[1].shape[0], peaks[2].shape[0])

    #for i in range(1,length):
    for i in range(length):
        """
        fig.get_axes()[0].scatter(
                peaks[0][i-1:i+1,0],
                peaks[1][i-1:i+1,0],
                peaks[2][i-1:i+1,0],
                c=color[peaks[0][i-1,1]])
        """
        fig.get_axes()[0].scatter(
                peaks[0][i,0],
                peaks[1][i,0],
                peaks[2][i,0],
                c=color[peaks[0][i,1]])

    for i in range(num_plots):
        ax = fig.get_axes()[i+1]
        ax.plot(peaks[i][:,1], peaks[i][:,0], c=beta_colors[int(beta[i]/MAX_BETA*1000)])

ax = fig.get_axes()[-1]
cb = mpl.colorbar.ColorbarBase(ax, cmap=plt.cm.viridis, orientation='horizontal')

plt.subplots_adjust(hspace=0.97, wspace=0.97, left=0, bottom=0, right=0.97, top=0.93)
plt.show()
