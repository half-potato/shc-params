from models.chunking2d import *
import math, util, os, params
import numpy as np
from icecream import ic

import matplotlib.pyplot as plt
#import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.gridspec as gridspec
#mpl.rcParams["savefig.directory"] = os.chdir(os.path.dirname(os.path.realpath(__file__)))

DEFAULT_CONFIG = {
        "control": (0.5, 0.5, 0.5, 1, 0.5, 1.5, 1),
        "noise_mag": 0.01,
        "n": 3,
        "m": 3,
        "beta": 0.01,
        "tao": 0.7,
        "theta": 0.1,
}

BOUNDING_X_CONFIG = {
        #"control": (0,0,0,1, 0, 0, 1),
        "control": (-0.1,-0.1,-0.1,1,0.1,0.1,1),
        "noise_mag": 0.01,
        "n": 3,
        "m": 3,
        "beta": 0.01,
        "tao": 0.7,
        "theta": 0.1,
}

BOUNDING_Z_CONFIG = {
        #"control": (0,0,0,1, 0, 0, 1),
        "control": (0.1,0.1,0.1,1,-0.4,-0.4,1),
        "noise_mag": 0.01,
        "n": 3,
        "m": 3,
        "beta": 0.01,
        "tao": 0.7,
        "theta": 0.1,
}


class ChunkingTester:
    def __init__(self, config):
        self.datas = [[], [], []]
        # generate model from config
        self.n = config["n"]
        self.m = config["m"]
        sigma = 2*np.random.rand(self.n, self.m)+5
        roe, epsilon, c = params.gen_params([self.n, self.m], sigma, controls=config["control"])
        self.model = Model(sigma, roe, epsilon, config["beta"], 
                           config["tao"],config["theta"], self.m, self.n,
                           mag=config["noise_mag"])

    def run(self, duration, dt):
        self.dt = dt
        self.space = np.linspace(0, duration, duration/dt)
        for i in self.space:
            #xs = np.vstack((xs, model.x))
            self.datas[0].append(np.copy(self.model.x))
            self.datas[1].append(np.copy(self.model.y))
            self.datas[2].append(np.copy(self.model.z))
            self.model.update(dt)
            if int(i/dt) % 1000 == 0:
                ic(self.model.x)
                print(i/dt)
        self.datas[0] = np.array(self.datas[0])
        self.datas[1] = np.array(self.datas[1])
        self.datas[2] = np.array(self.datas[2])

    #TODO: Add analysis
    def is_dead(self):
        return (self.model.x == 0).any() or (self.model.y == 0).any() or (self.model.z == 0).any()

    def get_eigen_values(self):
        # X nodes
        x_eigen = np.ones((self.n, self.n, self.m))
        roe = self.model.l_roe
        eps = self.model.epsilon
        sig = self.model.sigma
        b = self.model.beta
        for i in range(self.n):
            for j in range(self.n):
                for k in range(self.m):
                    x_eigen[i, j, k] = (roe[k, i, i]*sig[j,i] - 
                                        roe[k, j, i]*sig[i,i]) /  \
                                        (b*sig[i,i] + eps[k,k]*roe[k, i, i])
        
        # eigen for each y node at each x dominant mode
        y_eigen = np.ones((self.n, self.m, self.m))
        for i in range(self.n):
            for j in range(self.m):
                for k in range(self.m):
                    y_eigen[i, j, k] = (b*sig[i,i] + 
                                     eps[k,k]*roe[k, i, i] -
                                     eps[k,j%self.m]*roe[k, i, i]) / \
                                    (b*sig[i,i] + eps[k,k]*roe[k, i, i])

        return x_eigen, y_eigen

    def get_v(self):
        x_eigen, y_eigen = self.get_eigen_values()
        x_v = np.ones((self.n, self.m))
        for i in range(self.n):
            for j in range(self.m):
                x_v[i, j] = -x_eigen[(i-1)%self.n, (i-1)%self.n, j] / \
                            x_eigen[(i+1)%self.n, (i+1)%self.n, j]

        y_v = np.ones((self.n, self.m))
        for i in range(self.n):
            for j in range(self.m):
                y_v[i, j] = -y_eigen[(i-1)%self.n, j, j] / \
                            y_eigen[(i+1)%self.n, j, j]
        return x_v, y_v

    def get_durations(self, trim_n_cycles_x, trim_n_cycles_y):
        # returns [x durations, y durations, x d_duration, y d_duration]
        xs = self.datas[0]
        ys = self.datas[1]

        x_dur = np.ones((self.n, self.m))
        x_d_dur = np.ones((self.n, self.m))
        for i in range(self.n):
            for k in range(self.m):
                p, v = util.get_durations(xs[20:, i, k], lambda x: x > 0.1)
                trimmed_peaks = p[min(trim_n_cycles_x, len(p)):]
                x_dur[i, k] = np.median(trimmed_peaks)
                if len(trimmed_peaks) < 2:
                    x_d_dur[i, k] = 0
                else:
                    x_d_dur[i, k] = np.median(trimmed_peaks[1:] - trimmed_peaks[:-1])

        y_dur = np.ones((self.m))
        y_d_dur = np.ones((self.m))
        for k in range(self.m):
            p, v = util.get_durations(ys[20:, k], lambda x: x > 0.1)
            ic(p, v)
            trimmed_peaks = p[min(trim_n_cycles_y, len(p)):]
            y_dur[k] = np.mean(trimmed_peaks)
            if len(trimmed_peaks) < 2:
                y_d_dur[k] = 0
            else:
                y_d_dur[k] = np.median(trimmed_peaks[1:] - trimmed_peaks[:-1])

        return x_dur, y_dur, x_d_dur, y_d_dur

    def compile_report(self):
        report = {}
        report["is_dead"] = self.is_dead()
        x_dur, y_dur, x_d_dur, y_d_dur = self.get_durations(5, 1)
        report["x_dur"] = x_dur
        report["y_dur"] = y_dur
        report["x_d_dur"] = x_d_dur
        report["y_d_dur"] = y_d_dur
        x_eigen, y_eigen = self.get_eigen_values()
        report["x_eigen"] = x_eigen
        report["y_eigen"] = y_eigen
        x_v, y_v = self.get_v()
        report["x_v"] = x_v
        report["y_v"] = y_v
        return report

    def graph(self):
        #plt.clf()
        rowsize = (1, 4)
        #for i in self.datas[0]:
        ys = self.datas[1]
        ax = util.subplot2grid(plt, (0, 0), rowsize, self.m+1, 0)
        #ax.set_xlabel("Time")
        ax.set_ylabel("Y")
        y_palette = util.create_color_palette(self.m)
        for j in range(self.m):
            ax.plot(self.space, ys[:, j], c=y_palette[j], label="Y:%i" % j)
        ax.legend()
        ax.set_xticklabels([])
        ticks = [0, 1]
        ax.set_yticks(ticks)
        ax.set_yticklabels(ticks)
        ax.set_ylim(ymax=1)
        xs = self.datas[0]
        palette = util.create_color_palette(self.n)
        for k in range(self.m):
            ax = util.subplot2grid(plt, (0, 0), rowsize, self.m+1, k+1)
            #ax.set_xlabel("Time")
            ax.set_ylabel("X for Y%i" % k)
            ticks = [0, 2, 4]
            ax.set_yticks(ticks)
            ax.set_yticklabels(ticks)
            ax.set_ylim(ymax=ticks[-1])
            for j in range(self.n):
                ax.plot(self.space, xs[:, j, k], c=palette[j], label="X:%i" % j)
            if k == self.m -1:
                ax.set_xlabel("Time")
                ax.legend()
            else:
                ax.set_xticklabels([])
        plt.show()

if __name__ == "__main__":
    #t = ChunkingTester(BOUNDING_Z_CONFIG)
    t = ChunkingTester(DEFAULT_CONFIG)
    t.run(100, 0.05)
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ic(t.compile_report())
    t.graph()
