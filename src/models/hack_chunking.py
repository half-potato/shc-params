import numpy as np
import math
from icecream import ic
from util import runge_kutta

class Model:
    def __init__(self, 
            sigma, l_roe):
        self.sigma=sigma
        self.l_roe=l_roe

        self.x = 0.2*np.random.rand(*self.l_roe.shape[1:])
        """
        """
        self.v = np.ones(self.l_roe.shape[3:])
        self.N_k = 3
        self.M = 3

    def x_prime(self, x):
        x_p = []
        for i in range(self.M):
            #i = 1
            # Column matrices k
            sum_inhibition = np.zeros_like(x[i])
            # l_roe is in k i j order
            for j in range(self.N_k):
                sum_inhibition += self.l_roe[i,j]*x[j]
            x_p.append(x[i]*(self.sigma[i]-sum_inhibition))
        #x_p = np.array(x_p)
        #temp = np.zeros_like(x_p)
        #temp[:,0] = x_p[:,0]
        #return temp
        return np.array(x_p)

    def update(self, dt):
        cx = self.x+runge_kutta(self.x, self.x_prime, dt)

        self.x = cx

    def js(self):
        return 0.04*np.sum(self.x, axis=1)

    def __repr__(self):
        return "x:\n%s\ny:\n%s\nz:\n%s\n" % (str(self.x), str(self.y), str(self.z))

    def __str__(self):
        mats = "σ:\n%s\nρ:\n%s\n" % (str(self.sigma), str(self.l_roe))
        var = "x:\n%s\n" % (str(self.x))
        return ("%s\n" * 2) % (mats, var)
