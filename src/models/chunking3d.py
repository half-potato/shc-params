from util import runge_kutta
import numpy as np
from icecream import ic
import math

class Matrices:
    def __init__(self, sigma, l_roe, l_epsilon, l_sigma):
        self.sigma = np.array(sigma)
        self.l_roe = np.array(l_roe)
        self.l_epsilon = np.array(l_epsilon)
        self.l_sigma = np.array(l_sigma)

    def __repr__(self):
        syms = ["σ","ρ","ξ","ς"]
        variables = [str(i) for i in [self.sigma, self.l_roe,self.l_epsilon,self.l_sigma]]
        zipper = [None] * len(syms) * 2
        zipper[::2] = syms
        zipper[1::2] = variables
        return "%s:\n%s\n"*len(syms) % tuple(zipper)

class Params:
    def __init__(self, beta, delta, tao, T, theta, l_theta):
        self.beta = beta
        self.delta = delta
        self.tao = tao
        self.T = T
        self.theta = theta
        self.l_theta = l_theta

    def __repr__(self):
        syms = ["β","δ","τ","T","θ","Θ"]
        variables = [str(i) for i in [self.beta, self.delta, self.tao, self.T, self.theta, self.l_theta]]
        zipper = [None] * len(syms) * 2
        zipper[::2] = syms
        zipper[1::2] = variables
        return "%s:%s\n"*len(syms) % tuple(zipper)

class Model:
    def __init__(self, matrices, params):
        self.matrices = matrices
        self.params = params
        self.x = np.random.rand(*self.matrices.l_roe.shape[1:])
        self.y = np.random.rand(*self.matrices.l_roe.shape[2:])
        self.z = np.random.rand(*self.matrices.l_roe.shape[2:])
        self.v = np.random.rand(*self.matrices.l_roe.shape[3:])
        #self.v = np.zeros(self.matrices.l_roe.shape[3:])*0.001
        #self.v[0] = 1
        self.w = np.random.rand(*self.matrices.l_roe.shape[3:])

    def x_prime(self, x):
        x_p = []
        for i in range(self.matrices.l_roe.shape[0]):
            sum_inhibition = np.zeros_like(x[i,:,:])
            for j in range(self.matrices.l_roe.shape[1]):
                sum_inhibition += self.matrices.l_roe[i,j,:,:]*x[j,:,:]
            x_p.append(x[i,:,:]*(self.matrices.sigma[i,:,:]*self.y-sum_inhibition))
        return np.array(x_p)

    def y_prime(self, y):
        return y*(self.v - self.params.beta*np.sum(self.x, axis=0) - self.z)/self.params.tao

    def z_prime(self, z):
        z_p = []
        for l in range(self.matrices.l_epsilon.shape[0]):
            sum_excitation = np.zeros_like(z[:,l])
            for m in range(self.matrices.l_epsilon.shape[1]):
                sum_excitation += self.matrices.l_epsilon[l,m,:]*self.y[m,l]
            z_p.append((sum_excitation - z[:,l])/self.params.l_theta)

        z_p = np.array(z_p).T
        """
        # Check work (verified)
        out = np.zeros_like(z)
        for k in range(z.shape[0]):
            for l in range(z.shape[1]):
                sum_excitation = 0
                for m in range(self.matrices.l_epsilon.shape[1]):
                    sum_excitation += self.matrices.l_epsilon[l,m,k]*self.y[m,l]
                out[k,l] = (sum_excitation-z[k,l])/self.params.l_theta
        ic(z_p, out)
        return out #np.array(z_p).T
        """
        return z_p

    def v_prime(self, v):
        return v*(np.ones_like(v) - self.params.delta*np.sum(self.y, axis=0) - self.w)/self.params.T

    def w_prime(self, w):
        sum_excitation = np.zeros_like(w)
        for q in range(self.matrices.l_sigma.shape[0]):
            sum_excitation += self.matrices.l_sigma[q,:]*self.v[q]

        return (sum_excitation - w)/self.params.theta

    def update(self, dt):
        cx = self.x+runge_kutta(self.x, self.x_prime, dt)
        cy = self.y+runge_kutta(self.y, self.y_prime, dt)
        cz = self.z+runge_kutta(self.z, self.z_prime, dt)
        cv = self.v+runge_kutta(self.v, self.v_prime, dt)
        cw = self.w+runge_kutta(self.w, self.w_prime, dt)

        self.x = cx
        self.y = cy
        self.z = cz
        self.v = cv
        self.w = cw

    def __repr__(self):
        syms = ["x","y","z","v","w"]
        variables = [str(i) for i in [ self.x, self.y, self.z, self.v, self.w ]]
        zipper = [None] * len(syms) * 2
        zipper[::2] = syms
        zipper[1::2] = variables
        return "%s:\n%s\n"*len(syms) % tuple(zipper)
