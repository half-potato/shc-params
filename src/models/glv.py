import numpy as np
import math
from icecream import ic

# Returns approximate change in x
def runge_kutta(x, derivative, step_size):
    k1 = derivative(x)*step_size
    k2 = derivative(x + k1/2)*step_size
    k3 = derivative(x + k2/2)*step_size
    k4 = derivative(x + k3)*step_size
    return (k1+2*k2+2*k3+k4)/6

class Model:
    def __init__(self, sigma, l_roe, mag=0):
        self.sigma = sigma.astype(np.longdouble)
        self.l_roe = l_roe.astype(np.longdouble)
        self.n = self.l_roe.shape[0]
        self.x = 0.2*np.random.rand(self.n).astype(np.longdouble)
        self.mag = mag
        self.r = np.zeros((self.n)).astype(np.longdouble)
        self.i = 0

    def x_prime(self, x):
        # Generate brownian noise
        self.i += 1
        self.r += np.random.normal(0, 1, self.n) * self.mag
        #ic(self.sigma*self.x, self.l_roe.dot(x)*self.x)
        #ic(self.sigma*self.x- self.l_roe.dot(x)*self.x)
        return self.x*(self.sigma - self.l_roe.dot(x) + self.r/math.sqrt(self.i))

    def update(self, dt):
        self.x += runge_kutta(self.x, self.x_prime, dt)

    def __repr__(self):
        return "x:\n%s\nσ:\n%s\nρ:\n%s\n" % (str(self.x), str(self.sigma), str(self.l_roe))
