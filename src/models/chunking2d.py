import numpy as np
import math
from icecream import ic
from util import runge_kutta

class Model:
    def __init__(self, 
            sigma, l_roe, epsilon, # matrices
            beta, tao, l_theta,
            M, N_k, mag=0): # params
        self.sigma=sigma
        self.l_roe=l_roe
        self.epsilon=epsilon
        self.beta=beta
        self.tao=tao
        self.l_theta=l_theta

        self.N_k = N_k
        self.M = M
        self.x = 0.2*np.random.rand(N_k, M)
        self.y = np.random.rand(M)
        self.z = np.random.rand(M)
        self.v = np.ones(M)
        self.t = 0

    def x_prime(self, x):
        x_p = []
        for i in range(self.N_k):
            # Column matrices k
            sum_inhibition = np.zeros_like(x[i])
            # l_roe is in k i j order
            for j in range(self.l_roe.shape[1]):
                sum_inhibition += self.l_roe[:,i,j]*x[j]
            x_p.append(x[i]*(self.sigma[i]*self.y-sum_inhibition))# + x[i]*np.random.rand(*x[i].shape))
        return np.array(x_p)

    def y_prime(self, y):
        return y*(self.v - self.beta*np.sum(self.x, axis=0) - self.z)/self.tao

    def z_prime(self, z):
        sum_excitation = np.zeros_like(z)
        for m in range(self.M):
            sum_excitation += self.epsilon[m]*self.y[m]

        return (sum_excitation - z)/self.l_theta

    def update(self, dt):
        cx = self.x+runge_kutta(self.x, self.x_prime, dt)
        cy = self.y+runge_kutta(self.y, self.y_prime, dt)
        cz = self.z+runge_kutta(self.z, self.z_prime, dt)

        self.x = cx
        self.y = cy
        self.z = cz
        self.t += dt


    def js(self):
        return self.y + 0.04*np.sum(self.x, axis=0)

    def __repr__(self):
        return "x:\n%s\ny:\n%s\nz:\n%s\n" % (str(self.x), str(self.y), str(self.z))

    def __str__(self):
        mats = "σ:\n%s\nρ:\n%s\nξ:\n%s\n" % (str(self.sigma), str(self.l_roe), str(self.epsilon))
        params = "β:%s\nτ:%s\nθ:%s\n" % (str(self.beta), str(self.tao), str(self.l_theta))
        var = "x:\n%s\ny:\n%s\nz:\n%s\n" % (str(self.x), str(self.y), str(self.z))
        return ("%s\n" * 3) % (mats, params, var)
