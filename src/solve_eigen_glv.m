%{
Results for identity
v =
 
[ 1, 1, 1]
[ 1, 0, 0]
[ 0, 1, 0]
 
 
ans =
 
 sig2 - roe2_1*sig1
 sig3 - roe3_1*sig1
              -sig1
Results for no identity
v =
 
[ 1, 1, 1]
[ 1, 0, 0]
[ 0, 1, 0]
 
 
ans =
 
x2: (roe1_1*sig2 - roe2_1*sig1)/roe1_1
x3: (roe1_1*sig3 - roe3_1*sig1)/roe1_1
x1:                              -sig1
%}
clear
N = 3;

%% Declare variables
roe = sym('roe', [N N]); % to, from
sigma = sym('sig', [N 1]);
x = sym('x', [N 1]);

%% Identity
%%{
for i = 1:N
  roe(i, i) = 1;
end
%%}

%% Create equations
roe
x
roe*x
dxdt = x.*(sigma - roe*x);

%% Calculate jacobian for eigen values
linear = jacobian(dxdt, x);

%% Calculate eq points (not useful, need dominant mode assumption)
%sols = solve(dxdt==zeros(size(dxdt)), x);

%% Sub eq point
x_0 = sym('x', [N 1]);

syms x_a;

x_0(:,:) = 0;


x_0(1,1) = sigma(1,1)/roe(1, 1);

%% Calculate manual eigen value
subbed = subs(linear, [x], [x_0])
%{
syms lambda;
mat = det(subbed-eye(size(subbed))*lambda)
simp = simplify(mat,'Steps',50,'IgnoreAnalyticConstraints',true)
%}

[v d] = eig(subbed);

%% Get Eigen Values
s = size(v);
for i = 1:s(1)
    for j = 1:s(2)
        if v(i,j) ~= 0
            v(i,j) = 1;
        end
    end
end
v
diag(d)
