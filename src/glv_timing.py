from models.glv import *
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from icecream import ic
import util
import params

n = 5
RUNS = 10
SECONDS = 10000
MAX_DT = 0.1
MIN_DT = 0.01
TRIM_CYCLES = 0

sigma = 5*np.random.rand(n)+5

# Run model. If it runs into a nan, step up precision
def run(roe, dt):
    model = Model(sigma, roe)

    xs = np.zeros((n))
    space = range(SECONDS)
    for i in range(int(SECONDS/dt)):
        model.update(dt)
        xs = np.vstack((xs, model.x))
        if True in np.isnan(model.x):
            return run(roe, MIN_DT)
    return xs, dt

avg_peaks = []
d_peaks = []
controls = [[],[],[]]

for i in range(RUNS):
    # Gen model and run
    roe, control = params.gen_params([n], sigma)
    ic(control)
    xs, dt = run(roe, MAX_DT)
    
    # Analyze durations
    avg_peak, d_peak = util.get_avg_peak_and_peak_changes(
            xs, lambda x: x>1, TRIM_CYCLES, 0, dt)
    ic(avg_peak)

    avg_peaks.append(avg_peak)
    d_peaks.append(d_peak)
    for j, v in enumerate(control):
        controls[j].append(control[j])

    """
    fig = plt.figure()
    ax2 = fig.add_subplot(111)
    for i in range(n):
        ax2.plot(space, xs[1:,i],  c=util.get_color(i*40+50))
    plt.show()

    """

# Plot
fig = plt.figure()

avg_peaks = np.array(avg_peaks)
ic(avg_peaks.shape)
d_peaks = np.array(d_peaks)
ic(d_peaks.shape)
# Three plt per a control, i-1, i, i+1
for i, control in enumerate(controls):
    c = np.array(control)
    for j, offset in enumerate([-1,0,1]):
        ax = fig.add_subplot(3,3,i*3+j+1)
        ax.set_title("Control %i with offset %i" % (i+1, offset))
        ax.set_xlabel("r%i" % (i+1))
        ax.set_ylabel("Avg Peak")
        for k in range(n):
            ax.scatter(c[:,k], avg_peaks[:,(k+offset)%n], c=util.get_color(k*40+50))

plt.show()


