from models.glv import *
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from icecream import ic
import util, math, params 

DEFAULT_CONFIG = {
        "control": (0.5, 0.2, 0.5, 2),
        "noise_mag": 0.01,
        "n": 20,
}

DEAD_CONFIG = {
        "control": (1.1, 0.2, 0.5, 1),
        "noise_mag": 0.0,
        "n": 3,
}

BOUNDING_CONFIG = {
        "control": (-0.1, -0.1, -0.1, 1),
        "noise_mag": 0.001,
        "n": 3,
}

class GLVTester:
    def __init__(self, config):
        self.datas = [[]]
        # generate model from config
        self.n = config["n"]
        sigma = 2*np.random.rand(self.n)+5
        roe, c = params.gen_params([self.n], sigma, controls=config["control"])
        ic(c)
        self.model = Model(sigma, roe, mag=config["noise_mag"])

    def run(self, duration, dt):
        self.dt = dt
        self.space = np.linspace(0, duration, duration/dt)
        for i in self.space:
            #xs = np.vstack((xs, model.x))
            self.datas[0].append(np.copy(self.model.x))
            self.model.update(dt)
            if int(i/dt) % 1000 == 0:
                print(i/dt)
        self.datas[0] = np.array(self.datas[0])

    #TODO: Add analysis
    def is_dead(self):
        return (self.model.x == 0).any()

    def get_eigen_values(self):
        # X nodes
        x_eigen = np.ones((self.n, self.n))
        roe = self.model.l_roe
        sig = self.model.sigma
        # eigen for each y node at each x dominant mode
        for i in range(self.n):
            for j in range(self.n):
                x_eigen[i, j] = (roe[i, i]*sig[j] -
                                 roe[j, i]*sig[i])/roe[i, i]

        return x_eigen

    def get_v(self):
        x_v = np.ones((self.n))
        roe = self.model.l_roe
        sigma = self.model.sigma
        # Calculate v
        for i in range(self.n):
            # Iterate over every saddle
            i_next = (i+1)%self.n
            i_prev = (i-1)%self.n
            lambda_u = sigma[i_next] - roe[i_next, i]*sigma[i]
            v = (roe[i_prev, i]*sigma[i] - sigma[i_prev]) / lambda_u
            x_v[i] = v
            """
            s = 0
            h = 0
            for j in range(n):
                if j in [i, (i+1)%n]:
                    continue
                v2 = -(sigma[j] - roe[j, i]*sigma[i] ) / lambda_u
                #ic(v2)
                s += v2
                h += 1
            ic(i, s/h)
            """
        return x_v

    def get_durations(self, trim_n_cycles):
        # returns [x durations, y durations, x d_duration, y d_duration]
        xs = self.datas[0]

        x_dur = np.ones((self.n))
        x_d_dur = np.ones((self.n))
        for k in range(self.n):
            p, v = util.get_durations(xs[20:, k], lambda x: x > 0.1)
            trimmed_peaks = p[min(trim_n_cycles, len(p)):]
            x_dur[k] = np.mean(trimmed_peaks)
            if len(trimmed_peaks) < 2:
                x_d_dur[k] = 0
            else:
                x_d_dur[k] = np.median(trimmed_peaks[1:] - trimmed_peaks[:-1])

        return x_dur, x_d_dur

    def compile_report(self):
        report = {}
        report["is_dead"] = self.is_dead()
        x_dur, x_d_dur = self.get_durations(0)
        report["dur"] = x_dur
        report["d_dur"] = x_d_dur
        x_eigen = self.get_eigen_values()
        report["x_eigen"] = x_eigen
        x_v = self.get_v()
        report["x_v"] = x_v
        return report

    def graph(self, ax):
        #plt.clf()
        for i in self.datas:
            xs = np.array(i)
            palette = util.create_color_palette(self.n)
            for j in range(self.n):
                ax.set_xlabel("Time")
                ax.set_ylabel("Value")
                ax.plot(self.space, xs[:, j], c=palette[j], label="X:%i" % j)
            ticks = [0, 2, 4]
            ax.set_yticks(ticks)
            ax.set_yticklabels(ticks)
            ax.set_ylim(ymax=ticks[-1])
            ax.legend()
            plt.show()

if __name__ == "__main__":
    #t = GLVTester(BOUNDING_CONFIG)
    t = GLVTester(DEFAULT_CONFIG)
    t.run(1000, 0.1)
    ic(t.compile_report())
    fig = plt.figure()
    ax = fig.add_subplot(111)
    t.graph(ax)
