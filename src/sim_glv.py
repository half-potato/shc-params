from models.glv import *
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from icecream import ic
import util, params, math

n = 20
dt = 0.05
time = 10000

sigma = 2*np.random.rand(n)+5
control = (2, -2.5, 1.5, 1) # params with < 1 v yet stable
control = (2, 0.2, 1.5, 1) # params with < 1 v yet stable
#control = (2, 0.8, 1.5) # params with < 1 v yet stable
roe, _ = params.gen_params([n], sigma, controls=control)
model = Model(sigma, roe, mag=0.1)
#model.x = 50*np.ones((n))
print(model)

#xs = np.zeros((n))
xs = []
space = np.linspace(0, time, time/dt)
for i in space:
    #xs = np.vstack((xs, model.x))
    xs.append(np.copy(model.x))
    model.update(dt)
    if int(i/dt) % 1000 == 0:
        ic(model.r/math.sqrt(model.i))
        ic(model.x)
        print(i/dt)

xs = np.array(xs)
fig = plt.figure()
ax2 = fig.add_subplot(111)
palette = util.create_color_palette(n)
for i in range(n):
    ax2.set_xlabel("Time")
    ax2.set_ylabel("Value")
    ax2.plot(space/dt, xs[:,i], c=palette[i], label="X:%i" % i)
ax2.legend()
plt.show()
