from matplotlib import colors as mcolors
from icecream import ic
import numpy as np
import math, os
import matplotlib as mpl
__DIR_PATH__ = os.path.dirname(os.path.realpath(__file__))
mpl.rcParams["savefig.directory"] = os.chdir(os.path.join(__DIR_PATH__, "../images"))

def get_size(origin, rowsize, num_rows):
    return (rowsize[0]*num_rows+origin[0],rowsize[1]+origin[1])

def subplot2grid(plt, origin, rowsize, num_rows, i):
    return plt.subplot2grid(
            get_size(origin, rowsize, num_rows),
            (origin[0]+rowsize[0]*i, origin[1]), 
            rowspan=rowsize[0], colspan=rowsize[1])

def permute(min, max, shape):
    n = int(math.pow(shape[0], 1./shape[1]))
    buildingblock = np.linspace(min, max, n)[:,np.newaxis]
    out = np.linspace(min, max, n)[:,np.newaxis]
    for i in range(shape[1]-1):
        out = np.hstack((np.tile(out, (n,1)), np.repeat(buildingblock, n**(i+1), axis=0)))
    return out

def plot3d_col(data, color_dat, ax, colors):
    for i in range(data.shape[0]):
        color = colors[color_dat[i]]
        if len(colors) == 3:
            color = np.hstack((color, [1]))
        ax.scatter(
                data[i,0],
                data[i,1],
                data[i,2],
                c=color)

def get_durations(data, threshold_fn):
    # return [[duration, starttime]]
    peak_durations = []
    valley_durations = []
    start_i = -1
    end_i = -1
    for i in range(len(data)):
        if i+1 == len(data):
            continue
        # Peaks
        if not threshold_fn(data[i]) and threshold_fn(data[i+1]):
            start_i = i
            continue
        # Doesn't start collecting valleys until after the first peak in done
        if start_i != -1 and threshold_fn(data[i-1]) and not threshold_fn(data[i]):
            peak_durations.append([i - start_i - 1, start_i])
            end_i = i
            continue

        if end_i != -1 and start_i != -1 and start_i > end_i:
            valley_durations.append([start_i - end_i + 1, end_i])
            end_i = -1

    return np.array(peak_durations), np.array(valley_durations)

def get_avg_peak_and_peak_changes(data, threshold_fn, trim_n_cycles, def_val, dt):
    modes = data.shape[1]
    length = data.shape[0]

    peaks = []
    for i in range(modes):
        p, v = get_durations(data[20:,i], lambda x: x > 0.5)
        if p.shape[0] == 0:
            peaks.append(None)
        else:
            peaks.append(p)

    avg_peak = [0]*modes
    d_peak = [0]*modes
    for i, v in enumerate(peaks):
        if v is None:
            avg_peak[i] = def_val
            d_peak[i] = def_val
            continue

        if len(v) <= trim_n_cycles:
            avg_peak[i] = def_val
        else:
            avg_peak[i] = dt*np.median(v[trim_n_cycles:,0])

        # Changes in duration
        if len(v) <= trim_n_cycles+1:
            d_peak[i] = def_val
        else:
            d_peak[i] = dt*np.median(v[trim_n_cycles+1:,0] - v[trim_n_cycles:-1,0])

    return avg_peak, d_peak


# Returns approximate change in x
def runge_kutta(x, derivative, step_size):
    k1 = derivative(x)*step_size
    k2 = derivative(x + k1/2)*step_size
    k3 = derivative(x + k2/2)*step_size
    k4 = derivative(x + k3)*step_size
    return (k1+2*k2+2*k3+k4)/6

if __name__ == "__main__":
    print("Testing")
    test = [0,0,0,0,0,1,0,0,0,1,1,1,0,0,1,1]
    print("answer: 1, 3; 3, 2")
    print(get_durations(test, lambda x: x == 1))

COLORS = dict(mcolors.BASE_COLORS, **mcolors.CSS4_COLORS)
BY_HSV = sorted((tuple(mcolors.rgb_to_hsv(mcolors.to_rgba(color)[:3])), name)
                for name, color in COLORS.items())
min_sat = 100./255
min_hue = 0
min_val = 150./255
HSV_FILT = filter(lambda x: x[0][0] > min_hue and x[0][1] > min_sat and x[0][2] > min_val, BY_HSV)
SORTED_NAMES = [name for hsv, name in HSV_FILT]

def get_color(i):
    return COLORS[SORTED_NAMES[int(i)%len(SORTED_NAMES)]]

def create_color_palette(n):
    hue_di = (1-min_hue)/(n+1)/2
    last_hue = min_hue
    cols = []
    for (hsv, lab) in BY_HSV:
        if hsv[0] - last_hue > hue_di and lab in SORTED_NAMES:
            cols.append(lab)
            last_hue = hsv[0]

    return cols
