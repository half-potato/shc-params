%{
Results for no identity
v =
 
[ 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1]
[ 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0]
[ 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0]
[ 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
[ 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
[ 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
[ 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0]
[ 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0]
[ 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0]
[ 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1]
[ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0]
[ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0]
[ 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1]
[ 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1]
[ 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1]
 
 
ds =

-1
-1
0
0
0
0
0
0
(roe1_1_1*sig2_1 - roe2_1_1*sig1_1)/(b*sig1_1 + eps1_1*roe1_1_1)
(roe1_1_1*sig3_1 - roe3_1_1*sig1_1)/(b*sig1_1 + eps1_1*roe1_1_1)
(b*sig1_1 + eps1_1*roe1_1_1 - eps1_2*roe1_1_1)/(b*sig1_1 + eps1_1*roe1_1_1)
(b*sig1_1 + eps1_1*roe1_1_1 - eps1_3*roe1_1_1)/(b*sig1_1 + eps1_1*roe1_1_1)
long
long
long
 

Results for identity

v =
 
[ 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1]
[ 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0]
[ 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0]
[ 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
[ 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
[ 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
[ 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0]
[ 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0]
[ 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0]
[ 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1]
[ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0]
[ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0]
[ 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1]
[ 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1]
[ 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1]
 
 
ds =

-1
-1
0
0
0
0
0
0
(sig2_1 - roe2_1_1*sig1_1)/(b*sig1_1 + 1)
(sig3_1 - roe3_1_1*sig1_1)/(b*sig1_1 + 1)
(b*sig1_1 - eps2_1 + 1)/(b*sig1_1 + 1)
(b*sig1_1 - eps3_1 + 1)/(b*sig1_1 + 1)
long
long
long

%}
N = 3;
M = 3;

%% Declare variables
roe = sym('roe', [N N M]);
sigma = sym('sig', [N M]);
epsilon = sym('eps', [M M]);
%{
for i = 1:M
    epsilon(i,i) = 1;
end

for i = 1:M
    for j = 1:N
        roe(j,j,i) = 1;
    end
end
%}
syms b;
x = sym('x', [N M]);
y = sym('y', [1 M]);
z = sym('z', [1 M]);


%% Create equations
xs1 = squeeze(roe(:,:,1))*x(:,1);
xs2 = squeeze(roe(:,:,2))*x(:,2);
xs3 = squeeze(roe(:,:,3))*x(:,3);

dxdt = x.*(sigma.*(ones(size(y)).'*y) - [xs1 xs2 xs3]);
dydt = y.*(1-b*ones(1, N)*x - z);
dzdt = y*epsilon.' - z;

%% Calculate jacobian for eigen values
fns = [reshape(dxdt,1,[]), dydt, dzdt];
vars = [reshape(x,1,[]), y, z];
linear = jacobian(fns, vars);

%% Sub eq point 
x_0 = sym('x', [N M]);
y_0 = sym('y', [1 M]);
z_0 = sym('z', [1 M]);

syms x_a y_a z_a;

x_0(:,:) = 0;
y_0(:) = 0;

%{ 
%Identity constraint
x_0(1,1) = sigma(1,1)/(1+b*sigma(1,1));

y_0(1) = 1/(1+b*sigma(1,1));

z_0(1) = 1/(1+b*sigma(1,1));
z_0(2) = epsilon(2, 1)*y_0(1);
z_0(3) = epsilon(3, 1)*y_0(1);
%}

%%{ 
%No identity constraint
x_0(1,1) = sigma(1,1)/(epsilon(1,1) * roe(1,1,1) + b*sigma(1,1));

y_0(1) = x_0(1,1) * roe(1,1,1) / sigma(1,1);

z_0(1) = epsilon(1,1) * y_0(1);
z_0(2) = epsilon(1,2) * y_0(1);
z_0(3) = epsilon(1,3) * y_0(1);
%%}

%% Calculate manual eigen value
syms lambda;
subbed = subs(linear, [x y z], [x_0, y_0, z_0]);
mat = det(subbed-eye(size(subbed))*lambda);

[v d] = eig(subbed)
s = size(v);
for i = 1:s(1)
    for j = 1:s(2)
        if v(i,j) ~= 0
            v(i,j) = 1;
        end
    end
end
v

ds = diag(d)
