import numpy as np
from icecream import ic

def gen_controls(dims):
    if len(dims) == 1:
        # Fill roe
        n = dims[0]
        r1 = np.random.rand(n)+1
        # randomizes within range
        r2 = np.random.rand(n)
        r3 = np.random.rand(n)+1
        r4 = 4*np.random.rand(n)
        return (r1, r2, r3, r4)
    elif len(dims) == 2:
        # Fill roe
        n = dims[0]
        r1 = np.random.rand(n)*4
        # randomizes within range
        r2 = np.random.rand(n)
        r3 = np.random.rand(n)*4
        r4 = np.random.rand(n)*4
        r5 = np.random.rand(n)*2
        r6 = np.random.rand(n)*2
        r7 = np.random.rand(n)*2
        return (r1, r2, r3, r4, r5, r6, r7)

def gen_params(dims, sigma, controls=None, beta=0.01):
    if controls is None:
        controls = gen_controls(dims)

    cs = []
    for i, c in enumerate(controls):
        if type(c) != np.array:
            cs.append(c*np.ones(dims))
        else:
            cs.append(c)

    if len(dims) == 1:
        n = dims[0]
        roe = np.zeros((n,n))
        r1, r2, r3, r4 = cs
        ic(controls)

        # Fill roe
        for i in range(n):
            roe[i, i] = r4[i]
            min_roe = sigma[(i-1)%n]/sigma[i]
            max_roe = (sigma[(i-1)%n]+sigma[i])/sigma[i]
            #roe[(i-1)%n,i] = r1[i]*(max_roe-min_roe)+min_roe

            min_roe = r4[i]*sigma[(i-1)%n]/sigma[i]
            roe[(i-1)%n, i] = min_roe+r1[i]

            # Don't calculate v """
            #max_roe = r4[i]*sigma[(i+1)%n]/sigma[i]
            #roe[(i+1)%n, i] = max_roe-r2[i]
            # Don't calculate v """

        for i in range(n):
            for k in range(n):
                if k in [(i-1)%n, i, (i+1)%n]:
                    continue
                min_roe = r4[i]*sigma[k]/sigma[i]
                roe[k, i] = min_roe+r3[i]

        for i in range(n):
            # This min roe is invalid
            min_roe = r4[i]*sigma[(i-1)%n]/sigma[i]
            min_roe2 = r4[i]*(sigma[(i+1)%n] + sigma[(i-1)%n])/sigma[i] - roe[(i+1)%n, i]
            roe[(i-1)%n, i] = r2[i]+max(min_roe, min_roe2)

        """
        # Calculate v
        for i in range(n):
            # Iterate over every saddle
            i_next = (i+1)%n
            i_prev = (i-1)%n
            lambda_u = sigma[i_next] - roe[i_next, i]*sigma[i]
            v = (roe[i_prev, i]*sigma[i] - sigma[i_prev]) / lambda_u
            s = 0
            h = 0
            for j in range(n):
                if j in [i, (i+1)%n]:
                    continue
                v2 = -(sigma[j] - roe[j, i]*sigma[i] ) / lambda_u
                #ic(v2)
                s += v2
                h += 1
            ic(i, s/h)
        """

        return roe, controls
    elif len(dims) == 2:
        n, m = dims
        roe = np.repeat(np.zeros((n,n))[np.newaxis,:,:], m, axis=0)
        epsilon = np.zeros((m,m))

        # first 4 params are the same as glv, last 3 are for epsilon
        r1, r2, r3, r4, r5, r6, r7 = cs

        # Fill roe
        for k in range(m):
            for i in range(n):
                roe[k, i, i] = r4[i, k]
                min_roe = sigma[(i-1)%n, k]/sigma[i, k]
                max_roe = (sigma[(i-1)%n, k]+sigma[i, k])/sigma[i, k]
                #roe[(i-1)%n,i] = r1[i]*(max_roe-min_roe)+min_roe

                min_roe = r4[i, k]*sigma[(i-1)%n, k]/sigma[i, k]
                roe[k, (i-1)%n, i] = min_roe+r1[i, k]

                # Don't calculate v """
                #max_roe = r4[i]*sigma[(i+1)%n]/sigma[i]
                #roe[(i+1)%n, i] = max_roe-r2[i]
                # Don't calculate v """

        for k in range(m):
            for i in range(n):
                for j in range(n):
                    if j in [(i-1)%n, i, (i+1)%n]:
                        continue
                    min_roe = r4[i, k]*sigma[j, k]/sigma[i, k]
                    roe[k, j, i] = min_roe+r3[i, k]

        for k in range(m):
            for i in range(n):
                # This min roe is invalid
                min_roe = r4[i, k]*sigma[(i-1)%n, k]/sigma[i, k]
                min_roe2 = r4[i, k]*(sigma[(i+1)%n, k] + sigma[(i-1)%n, k])/sigma[i, k] - roe[k, (i+1)%n, i]
                roe[k, (i-1)%n, i] = r2[i, k]+max(min_roe, min_roe2)

        # Fill epsilon
        # Fill unstable manifolds
        for k in range(m):
            epsilon[k, k] = r7[0, k]
            mindex = np.argmin(sigma[:,k])
            cutoff = (r7[0, k]*r4[mindex, k] + beta*sigma[mindex,k]) / r4[mindex, k]
            epsilon[(k+1)%m, k] = cutoff - r5[0, k]
            #epsilon[(k-1)%m, k] = cutoff + r6[0, k]
            #v = epsilon[(k-1)%m, k]/epsilon[(k+1)%m, k]

        # Fill in stable manifolds
        for i in range(m):
            for k in range(m):
                    if k in [i, (i+1)%m]:
                        continue
                    mindex = np.argmin(sigma[:,k])
                    cutoff = (r7[0, k]*r4[mindex, k] + beta*sigma[mindex,k]) / r4[mindex, k]
                    epsilon[i, k] = cutoff + r6[i, k]
        # Impose v constraint
        for k in range(m):
            mindex = np.argmin(sigma[:,k])
            cutoff = (r7[0, k]*r4[mindex, k] + beta*sigma[mindex,k]) / r4[mindex, k]
            v_constraint = 2*beta*sigma[mindex,k]/r4[mindex,k] + 2*r7[0, k] - epsilon[k, (k+1)%m]
            ic(cutoff, v_constraint)
            epsilon[k, (k-1)%m] = max(cutoff, v_constraint) + r6[0, k]

        return roe, epsilon, controls
