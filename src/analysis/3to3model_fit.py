import numpy as np
from icecream import ic
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

MAX_BETA = 0.05

def plot3d_col(data, color_dat, ax, colors):
    for i in range(data.shape[0]):
        color = color_dat[i]
        ax.scatter(
                data[i,0],
                data[i,1],
                data[i,2],
                c=color)

# shape: count x 3
# 1 column for each Y mode
with open("../data/avg_peaks.npy", "rb") as f:
    avg_peaks = np.load(f)

# shape: count x 3
# 1 column for each Y mode
with open("../data/betas.npy", "rb") as f:
    betas = np.load(f)

ic(avg_peaks)
ic(betas)

N = betas.shape[0]
# clean data (remove 0s)
indices = []
for i in range(N):
    if 0 in avg_peaks[i,:]:
        indices.append(i)

avg_peaks_clean = np.delete(avg_peaks, indices, axis=0)
betas_clean = np.delete(betas, indices, axis=0)

M = betas_clean.shape[0]

W_est = np.linalg.pinv(betas_clean).dot(avg_peaks_clean).T
ic(W_est)

y_hat = W_est.dot(betas_clean.T).T
error_ind = (y_hat- avg_peaks_clean)**2
error = np.sqrt(np.sum(error_ind, axis=0))

ic(np.average(avg_peaks_clean, axis=0))
ic(error/M)

color_dat = np.clip(np.hstack((betas_clean/MAX_BETA,np.ones((M, 1)))), 0, 1)
beta_colors = plt.cm.viridis(np.linspace(0, 1, 1000))

ax1 = plt.subplot(211, projection='3d')
ax1.set_title("Predicted space")
ax1.set_xlabel("ΔT Y1")
ax1.set_ylabel("ΔT Y2")
ax1.set_zlabel("ΔT Y3")
ax2 = plt.subplot(212, projection='3d')
ax2.set_title("Time space")
ax2.set_xlabel("ΔT Y1")
ax2.set_ylabel("ΔT Y2")
ax2.set_zlabel("ΔT Y3")
plot3d_col(avg_peaks_clean, color_dat, ax2, beta_colors)
plot3d_col(y_hat, color_dat, ax1, beta_colors)
plt.show()
