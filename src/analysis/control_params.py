import numpy as np
import math
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
plt.subplots_adjust(left=0, bottom=0, right=0.97, top=0.93)

with open("../data/avg_peaks_controlled.npy", "rb") as f:
    avg_peaks = np.load(f)
with open("../data/d_peaks_controlled.npy", "rb") as f:
    d_peaks = np.load(f)
with open("../data/rs_controlled.npy", "rb") as f:
    rs = np.load(f)
with open("../data/r2s_controlled.npy", "rb") as f:
    r2s = np.load(f)
with open("../data/r3s_controlled.npy", "rb") as f:
    r3s = np.load(f)

# Plot 3d projection
#plt.subplot2grid((4,6), (0,0), colspan=2, rowspan=2, projection='3d')
#ax3 = plt.get_axes()[0]
ax1 = plt.subplot(221, projection='3d')
ax1.set_title("Change in peak size wrt r2, ps=peak size")
ax1.set_xlabel("ps Y mode 1")
ax1.set_ylabel("ps Y mode 2")
ax1.set_zlabel("ps Y mode 3")
ax1.quiver(
        r2s[:,0], 
        r2s[:,1], 
        r2s[:,2],
        avg_peaks[:,0], 
        avg_peaks[:,1], 
        avg_peaks[:,2], 
        length=5)

ax2 = plt.subplot(222, projection='3d')
ax2.set_title("Change in peak size/dt wrt r2")
ax2.set_xlabel("ps/dt Y mode 1")
ax2.set_ylabel("ps/dt Y mode 2")
ax2.set_zlabel("ps/dt Y mode 3")
ax2.quiver(
        r2s[:,0], 
        r2s[:,1], 
        r2s[:,2], 
        d_peaks[:,0], 
        d_peaks[:,1], 
        d_peaks[:,2], 
        length=.08)

ax3 = plt.subplot(223, projection='3d')
ax3.set_title("Change in peak size wrt r3")
ax3.set_xlabel("ps Y mode 1")
ax3.set_ylabel("ps Y mode 2")
ax3.set_zlabel("ps Y mode 3")
ax3.quiver(
        r3s[:,0], 
        r3s[:,1], 
        r3s[:,2], 
        avg_peaks[:,0], 
        avg_peaks[:,1], 
        avg_peaks[:,2], 
        length=3)

ax4 = plt.subplot(224, projection='3d')
ax4.set_title("Change in peak size/dt wrt r3")
ax4.set_xlabel("ps/dt Y mode 1")
ax4.set_ylabel("ps/dt Y mode 2")
ax4.set_zlabel("ps/dt Y mode 3")
ax4.quiver(
        r3s[:,0], 
        r3s[:,1], 
        r3s[:,2], 
        d_peaks[:,0], 
        d_peaks[:,1], 
        d_peaks[:,2], 
        length=.05)

plt.show()

with open("../data/avg_peaks_controlled.npy", "wb+") as f:
    np.save(f, avg_peaks)
with open("../data/rs_controlled.npy", "wb+") as f:
    np.save(f, rs)
with open("../data/r2s_controlled.npy", "wb+") as f:
    np.save(f, r2s)
with open("../data/r3s_controlled.npy", "wb+") as f:
    np.save(f, r3s)

