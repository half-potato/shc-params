import numpy as np
import math
import matplotlib.pyplot as plt
from icecream import ic
from mpl_toolkits.mplot3d import Axes3D

graph_size = (3, 2)

#suffix = "_controlled_median"
suffix = "_controlled_no_lim"
#suffix = "_controlled_linear"
with open("../data/avg_peaks%s.npy"%suffix, "rb") as f:
    avg_peaks = np.load(f)
with open("../data/d_peaks%s.npy"%suffix, "rb") as f:
    d_peaks = np.load(f)
with open("../data/rs%s.npy"%suffix, "rb") as f:
    rs = np.load(f)
with open("../data/r2s%s.npy"%suffix, "rb") as f:
    r2s = np.load(f)
with open("../data/r3s%s.npy"%suffix, "rb") as f:
    r3s = np.load(f)

# Trim data according to test

# get r2s test
R2S_LIM = int(r2s.shape[0]/2)
r2s = r2s[:R2S_LIM]
peaks_r2s = avg_peaks[:R2S_LIM]
d_peaks_r2s = d_peaks[:R2S_LIM]

# get r3s test
r3s = r3s[:R2S_LIM]
peaks_r3s = avg_peaks[:R2S_LIM]
d_peaks_r3s = d_peaks[:R2S_LIM]

ic(r2s)

graphs = [
        ("", "R2", "P", r2s, peaks_r2s),
        ("", "R3", "P", r3s, peaks_r3s),
        ("", "R2", "dP/dt", r2s, d_peaks_r2s),
        ("", "R3", "dP/dt", r3s, d_peaks_r3s),
]

def fit_line(x, y):
    y_ = np.mean(y)
    x_ = np.mean(x)
    sum_xy = 0
    for i in range(len(x)):
        sum_xy += (x[i] - x_)*(y[i] - y_)
    m = sum_xy/np.sum((x-x_)**2)
    b = y_ - m*x_
    return m, b

dim = (graph_size[0]*len(graphs), graph_size[1])

for i, g in enumerate(graphs):
    ic((i*graph_size[0], graph_size[1]))
    ax = plt.subplot2grid(dim, (i*graph_size[0], 0),#graph_size[1]),
            colspan=graph_size[0], rowspan=graph_size[1])
    ax.set_title(g[0])
    ax.set_xlabel(g[1])
    ax.set_ylabel("Log(%s)"%g[2])
    colors = ['r', 'g', 'b']
    for j in range(3):
        ax.scatter(g[-2][:,j], np.log(g[-1][:,j]), c=colors[j], s=0.1)
        #ax.scatter(g[4][:,j], g[3][:,j], c=colors[j])

        # draw line of best fit
        # remove zeros
        zeros = np.argwhere(np.equal(g[-1][:,j], 0))
        y = np.delete(g[-1][:,j], zeros)
        x = np.delete(g[-2][:,j], zeros)
        log_y = np.log(y)
        nans = np.argwhere(np.isnan(log_y))
        log_y = np.delete(log_y, nans)
        x = np.delete(x, nans)

        m, b = fit_line(x, log_y)
        lspace = np.linspace(min(g[-2][:,j]), max(g[-2][:,j]), len(g[-2][:,j]))
        log_y_ = m*lspace + b
        err = (np.exp(log_y_) - np.mean(g[-1][:,j]))**2
        err = np.sum(err[~np.isnan(err)])
        variance = (g[-1][:,j] - np.mean(g[-1][:,j]))**2
        variance = np.sum(variance[~np.isnan(variance)])
        ic(err/variance)
        ic(err,variance)
        ax.plot(lspace, np.exp(log_y_), c=colors[j])

plt.show()
