import numpy as np
from icecream import ic
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

MAX_BETA = 0.05
GROUP_SIZE = 10
BETA_COLORS = plt.cm.viridis(np.linspace(0, 1, 1000))

def plot3d_col(data, color_dat, ax, colors):
    for i in range(data.shape[0]):
        color = color_dat[i]
        ax.scatter(
                data[i,0],
                data[i,1],
                data[i,2],
                c=color)

def plot3d_col_lines(data, color_dat, ax, colors):
    for i in range(1, data.shape[0]):
        color = color_dat[i]
        ax.plot(
                data[i-1:i+1,0],
                data[i-1:i+1,1],
                data[i-1:i+1,2],
                c=color)

# shape: count x 3
# 1 column for each Y mode
with open("../data/avg_peaks_exhaustive.npy", "rb") as f:
    avg_peaks = np.load(f)

# shape: count x 3
# 1 column for each Y mode
with open("../data/betas_exhaustive.npy", "rb") as f:
    betas = np.load(f)

ic(avg_peaks.shape)
ic(betas.shape)

N = betas.shape[0]

# clean data (remove 0s)
indices = []
for i in range(N):
    if 0 in avg_peaks[i,:]:
        indices.append(i)

avg_peaks_clean = np.delete(avg_peaks, indices, axis=0)
betas_clean = np.delete(betas, indices, axis=0)
M = betas_clean.shape[0]

color_dat = np.clip(np.hstack((betas_clean/MAX_BETA,np.ones((M, 1)))), 0, 1)

"""
ax1 = plt.subplot(221, projection='3d')
ax1.set_title("Time space")
ax1.set_xlabel("ΔT Y1")
ax1.set_ylabel("ΔT Y2")
ax1.set_zlabel("ΔT Y3")

# split data into groups for lines
# then graph them
for i in range(GROUP_SIZE**2):
    plot3d_col_lines(avg_peaks_clean[i*10:i*10+10], color_dat[i*10:i*10+10], ax1, BETA_COLORS)

ax2 = plt.subplot(222, projection='3d')
ax2.set_title("Time space")
ax2.set_xlabel("ΔT Y1")
ax2.set_ylabel("ΔT Y2")
ax2.set_zlabel("ΔT Y3")

# split data into groups for lines
# then graph them
color_dat = np.hstack((avg_peaks_clean/np.max(avg_peaks_clean),np.ones((M, 1))))
ic(color_dat.shape)

for i in range(GROUP_SIZE**2):
    plot3d_col_lines(betas_clean[i*10:i*10+10], color_dat[i*10:i*10+10], ax2, BETA_COLORS)
"""

ax3 = plt.subplot(211, projection='3d')
ax3.quiver(
        betas_clean[:,0], 
        betas_clean[:,1], 
        betas_clean[:,2], 
        avg_peaks_clean[:,0], 
        avg_peaks_clean[:,1], 
        avg_peaks_clean[:,2], 
        length=0.0003)
ax3.set_title("Vector field (ΔT1, ΔT2, ΔT3)")
ax3.set_xlabel("β1")
ax3.set_ylabel("β2")
ax3.set_zlabel("β3")

ax4 = plt.subplot(212, projection='3d')
ax4.quiver(
        avg_peaks_clean[:,0], 
        avg_peaks_clean[:,1], 
        avg_peaks_clean[:,2], 
        betas_clean[:,0], 
        betas_clean[:,1], 
        betas_clean[:,2], 
        length=10)
ax4.set_title("Vector field (β1, β2, β3)")
ax4.set_xlabel("ΔT1")
ax4.set_ylabel("ΔT2")
ax4.set_zlabel("ΔT3")

plt.show()
