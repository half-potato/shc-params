# for each dimension graph the range of the shape along it
import numpy as np
from icecream import ic
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

MAX_BETA = 0.05
GROUP_SIZE = 10

# shape: count x 3
# 1 column for each Y mode
with open("../data/avg_peaks_exhaustive.npy", "rb") as f:
    avg_peaks = np.load(f)

# shape: count x 3
# 1 column for each Y mode
with open("../data/betas_exhaustive.npy", "rb") as f:
    betas = np.load(f)

ic(avg_peaks.shape)
ic(betas.shape)

N = betas.shape[0]

# clean data (remove 0s)
indices = []
for i in range(N):
    if 0 in avg_peaks[i,:]:
        indices.append(i)

avg_peaks_clean = np.delete(avg_peaks, indices, axis=0)
betas_clean = np.delete(betas, indices, axis=0)
M = betas_clean.shape[0]

color_dat = np.clip(np.hstack((betas_clean/MAX_BETA,np.ones((M, 1)))), 0, 1)

