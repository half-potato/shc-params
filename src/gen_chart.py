from models.glv import *
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from icecream import ic
import util
import params

n = 4
dt = 0.05
time = 4000

sigma = 5*np.random.rand(n)+5
# 3 sets per, 1 for each param
controls = {
        "min": [
            (1, 0.5, 1.2),
            (2, -0.1, 1.2),
            (1.2, 0.5, 1),
        ],
        "safe": [
            (2, 0.5, 1.2),
            (2, 0.5, 1.2),
            (2, 0.5, 1.2),
        ],
        "max": [
            (3, 0.5, 1.2),
            (2, 1, 1.2),
            (2, 0.5, 2),
        ],
}

size = (3,3)

col = {
        "min": 0,
        "safe": 1,
        "max": 2,
}
x = None

for bound in controls:
    for i, control in enumerate(controls[bound]):
        name = "r1*%.2f, r2*%.2f, r3*%.2f" % control
        ax = plt.subplot2grid(size, (i, col[bound]), rowspan=1, colspan=1)
        control_arrays = []
        for j in control:
            control_arrays.append(np.ones((n))*j)
        roe, _ = params.gen_params([n], sigma, control_arrays)
        model = Model(sigma, roe)
        if x is None:
            x = np.copy(model.x)
        else:
            model.x = np.copy(x)

        xs = []
        space = np.linspace(0, time, time/dt)
        for i in space:
            model.update(dt)
            xs.append(np.copy(model.x))

        xs = np.array(xs)
        palette = util.create_color_palette(n)
        for k in range(n):
            ax.plot(space*dt, xs[:, k],  c=palette[k])
            ax.set_xlabel("Time")
            ax.set_ylabel("Mode")
            ax.set_title(name)
plt.show()


