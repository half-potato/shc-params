from models.chunking3d import *
import matplotlib.pyplot as plt
from icecream import ic
import numpy as np
import math
import util

### Initialize Variables

dt = 0.1
linspace = range(3000)

M = 3
N = 3
P = 3

SIGMA = np.array( [[6.94, 5.11, 8.94, 5.86, 8.33, 9.62],
                   [5.48, 5.66, 5.39, 9.89, 9.99, 5.82],
                   [7.65, 8.98, 9.21, 6.02, 5.71, 5.12],
                   [7.61, 7.73, 5.62, 7.93, 5.80, 5.39],
                   [5.11, 9.99, 5.52, 5.66, 5.50, 8.21],
                   [5.84, 9.39, 7.08, 5.16, 8.37, 6.87]] )
SIGMA = np.repeat(SIGMA[:,:,np.newaxis], P, -1)
SIGMA = np.random.rand(N,M,P)*5 + 5

roe = np.zeros((N,N,M,P))
for i in range(N):
    for k in range(M):
        for l in range(P):
            roe[(i-1)%N,i,k,l] = SIGMA[(i-1)%N, k, l]/SIGMA[i, k, l] + 0.51
            roe[(i+1)%N,i,k,l] = SIGMA[(i+1)%N, k, l]/SIGMA[i, k, l] - 0.5
            roe[i,i,k,l] = 1
            for i2 in range(N):
                if i2 in [(i-1)%N, i, (i+1)%N]:
                    continue
                roe[i2,i,k,l] = roe[i-1,i,k,l] + \
                                (SIGMA[i2,k,l]-SIGMA[i-1,k,l])/SIGMA[i,k,l] + 2

epsilon = np.zeros((P,M,M))
for k in range(M):
    epsilon[0,(k+1)%M,k] = 1.4
    epsilon[1,(k+1)%M,k] = 1.3
    epsilon[2,(k+1)%M,k] = 1.5
    for l in range(P):
        epsilon[l,k,k] = 1
        epsilon[l,(k-1)%M,k] = 0.5
        for k2 in range(M):
            if k2 in [(k-1)%M, k, (k+1)%M]:
                continue
            epsilon[l,k,k2] = epsilon[l,k,(k-1)%M] + 2

sigma = np.zeros((P,P))

for l in range(P):
    sigma[l,l] = 1
    sigma[(l-1)%P,l] = 0.5
    sigma[(l+1)%P,l] = 1.4

matrices = Matrices(SIGMA, roe, epsilon, sigma)

tao = 0.8
theta = 2.0
beta = 0.01
T = 5
THETA = 10
delta = 0.01

params = Params(beta, delta, tao, T, THETA, theta)

model = Model(matrices, params)
print(matrices)
#print(params)
#print(model)

xs = np.zeros_like(model.x)[np.newaxis]
ys = np.zeros_like(model.y)[np.newaxis]
vs = np.zeros_like(model.v)[np.newaxis]

# Run
for t in linspace:
    model.update(dt)
    #print(model)
    xs = np.vstack((xs, model.x[np.newaxis]))
    ys = np.vstack((ys, model.y[np.newaxis]))
    vs = np.vstack((vs, model.v[np.newaxis]))

# Plot
fig = plt.figure()
ax1 = fig.add_subplot(311)
print(xs.shape)
count = 0
for i in range(N):
    for j in range(M):
        for k in range(P):
            ax1.plot(linspace, xs[1:,i,j,k],  c=util.get_color(count*10))
            count += 1

ax2 = fig.add_subplot(312)
for j in range(M):
    for k in range(P):
        ax2.plot(linspace, ys[1:,j,k],  c=util.get_color((j*P+k)*10))

ax3 = fig.add_subplot(313)
for k in range(P):
    ax3.plot(linspace, vs[1:,k],  c=util.get_color(k*20+20))
plt.show()
