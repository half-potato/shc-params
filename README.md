# Investigating the chunking dynamics of LSTMs

# Ideas
- The winnerless competition dynamics often represent one hot vectors where each number is the activity of a state. Perhaps embedding can help study this.
- Embedding does a sort of compression like chunking. Is it possible to embed a large dimensional input using chunking
  - Woah
  - Maybe find an existing feature compression net
- Study the stable heteroclinic channel in iterated maps
- Study chunking in social groups (groups of people)

# Potential datasets
- Memorizing a chess board is the original chunking experiment. Experts were better able to remember it
